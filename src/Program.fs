﻿open System
open System.IO
open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Running
open Microsoft.FSharp.Core
open Microsoft.Research.Science.Data
open Argu
open KdTree

open FsKDTree
open Oceanbox.FvcomKit

module Data =
    let buksnes = "/data/Napp/Buksnes_waste_0156.nc"
    let LT3 = "/data/LT3/LT3_0001.nc"
    let uri =
        let x = NetCDF4.NetCDFUri ()
        x.FileName <- LT3
        x.OpenMode <- ResourceOpenMode.ReadOnly
        x
    let ds : DataSet =
        NetCDF4.NetCDFDataSet.Open uri
    let fvcomGrid =
        Fvcom.getGrid ds
    let grid : Grid.IGrid =
        fvcomGrid.ToGrid()
    let extGrid =
        grid
        |> Grid.ExtendedGrid

    module NIdx =
        let createIdx () =
            Grid.makeNeighborIndex grid

    module KdTreeV1 =
       let createNodeIdx () =
           let tree = KdTree<float, int>(2, KdTree.Math.DoubleMath())
           grid.getVertices ()
           |> Array.iteri (fun i v ->
               let x, y = v
               tree.Add([| x; y |], i) |> ignore
           )
           tree

       let createBalancedNodeIdx () =
           let tree = KdTree<float, int>(2, KdTree.Math.DoubleMath())
           grid.getVertices ()
           |> Array.iteri (fun i v ->
               let x, y = v
               tree.Add([| x; y |], i) |> ignore
           )
           tree.Balance ()
           tree


       let createElemIdx () =
           let tree = KdTree<float, int>(2, KdTree.Math.DoubleMath())

           grid.getCells ()
           |> Array.iteri (fun i _ ->
               let x, y =
                   grid.getCellVertices i
                   |> Grid.Util.Element.calcCentroid

               tree.Add([| x; y |], i) |> ignore
           )

           tree

       let createBalancedElemIdx () =
           let tree = KdTree<float, int>(2, KdTree.Math.DoubleMath())

           grid.getCells ()
           |> Array.iteri (fun i _ ->
               let x, y =
                   grid.getCellVertices i
                   |> Grid.Util.Element.calcCentroid

               tree.Add([| x; y |], i) |> ignore
           )

           tree.Balance ()

           tree


    module FsKDTreeV1 =

       let createNodeIdx () =
           grid.getVertices ()
           |> Array.mapi (fun i v ->
               let x, y = v
               { Pos = { X = x; Y = y }; Data = i })
           |> create2DTree (LeafNodeSize 64)

       let createElemIdx () =
           grid.getCells ()
           |> Array.mapi (fun i _ ->
               let x, y =
                   grid.getCellVertices i
                   |> Grid.Util.Element.calcCentroid
               { Pos = { X = x; Y = y }; Data = i })
           |> create2DTree (LeafNodeSize 64)

[<MemoryDiagnoser>]
type CreateBenchmarks () =
    [<Benchmark>]
    member _.NeighborIndexV1 () =
        Data.NIdx.createIdx ()

    [<Benchmark>]
    member _.CsNodeTreeV1 () =
        Data.KdTreeV1.createNodeIdx ()
    [<Benchmark>]
    member _.CsBalancedNodeTreeV1 () =
        Data.KdTreeV1.createBalancedNodeIdx ()

    [<Benchmark>]
    member _.CsElemTreeV1 () =
        Data.KdTreeV1.createElemIdx ()
    [<Benchmark>]
    member _.CsBalancedElemTreeV1 () =
        Data.KdTreeV1.createBalancedElemIdx ()

    [<Benchmark>]
    member _.FsNodeTreeV1 () =
        Data.FsKDTreeV1.createNodeIdx ()
    [<Benchmark>]
    member _.FsElemTreeV1 () =
        Data.FsKDTreeV1.createElemIdx ()

// TODO(simkir): move different searches to seperate benchmarks
[<MemoryDiagnoser>]
type SearchBenchmarks () =
    let radius = 300.0
    let searchPoint = [| 442708.0; 7525958.5 |]
    let searchPos : float Pos = { X = 442708.0; Y = 7525958.5 }

    let mutable csNodeTree = None
    let mutable csElemTree = None

    let mutable fsNodeTree = None
    let mutable fsElemTree = None

    member _.GetCsNodeTree() = csNodeTree.Value
    member _.GetCsElemTree() = csElemTree.Value
    member _.GetFsNodeTree() = fsNodeTree.Value
    member _.GetFsElemTree() = fsElemTree.Value

    [<GlobalSetup>]
    member _.Setup() =
        csNodeTree <- Data.KdTreeV1.createNodeIdx () |> Some
        csElemTree <- Data.KdTreeV1.createElemIdx () |> Some
        fsNodeTree <- Data.FsKDTreeV1.createNodeIdx () |> Some
        fsElemTree <- Data.FsKDTreeV1.createElemIdx () |> Some

        ()

    [<Benchmark>]
    member this.CsNodeRadialSearch () =
        let tree = this.GetCsNodeTree ()
        tree.RadialSearch(searchPoint, radius)

    [<Benchmark>]
    member this.CsElemRadialSearch () =
        let tree = this.GetCsElemTree ()
        tree.RadialSearch(searchPoint, radius)

    [<Benchmark>]
    member this.FsNodeRadialSearch () =
        let tree = this.GetFsNodeTree ()
        radialSearch tree 300.0 searchPos

    [<Benchmark>]
    member this.FsElemRadialSearch () =
        let tree = this.GetFsElemTree ()
        radialSearch tree 300.0 searchPos

    [<Benchmark>]
    member this.FsNodeRangeSearch () =
        let tree = this.GetFsNodeTree ()
        // TODO(simkir): This seems wrong
        let min = { X = 442708.0; Y = 7525958.5 }
        let max = { X = 432708.0; Y = 7425958.5 }

        rangeSearch tree min max

    [<Benchmark>]
    member this.FsElemRangeSearch () =
        let tree = this.GetFsElemTree ()
        // TODO(simkir): This seems wrong
        let min = { X = 442708.0; Y = 7525958.5 }
        let max = { X = 432708.0; Y = 7425958.5 }

        rangeSearch tree min max

    [<Benchmark>]
    member this.CsNodekNN () =
        let tree = this.GetCsNodeTree ()
        tree.GetNearestNeighbours(searchPoint, 1)

    [<Benchmark>]
    member this.CsElemkNN () =
        let tree = this.GetCsElemTree ()
        tree.GetNearestNeighbours(searchPoint, 1)

    [<Benchmark>]
    member this.FsNodekNN () =
        let tree = this.GetFsNodeTree ()
        nearestNeighbor tree searchPos

    [<Benchmark>]
    member this.FsElemkNN () =
        let tree = this.GetFsElemTree ()
        nearestNeighbor tree searchPos

type PickleBenchmarks () =
    let mutable idxTree = Data.FsKDTreeV1.createNodeIdx ()
    member _.IdxTree with get() = idxTree and set(t) = idxTree <- t

    [<Benchmark>]
    member this.V1WriteToDisk () =
        let b = FsKDTree.toBytes this.IdxTree
        File.WriteAllBytes("2DTree.data", b)

    [<Benchmark>]
    member this.V1ReadFromDisk () =
        let b = File.ReadAllBytes("2DTree.data")
        this.IdxTree <- FsKDTree.fromBytes<float, int> b

type Arguments =
    | Task of task: string
    | Function of func: string
    interface IArgParserTemplate with
        member this.Usage =
            match this with
            | Task _ -> "choose task: benchmark or test"
            | Function _ -> "choose function: create, search, pickle, unpickle"

[<EntryPoint>]
let main argv =
    let parser = ArgumentParser.Create<Arguments>(programName = "FsKDTree-cli")
    try
        let results = parser.Parse(inputs = argv, ignoreMissing = true)
        let task = results.GetResult Arguments.Task

        match task with
        | "benchmark" ->
            let func = results.GetResult Arguments.Function
            match func with
            | "create" ->
                BenchmarkRunner.Run<CreateBenchmarks>() |> ignore
            | "search" ->
                BenchmarkRunner.Run<SearchBenchmarks>() |> ignore
            | "pickle" ->
                BenchmarkRunner.Run<PickleBenchmarks>() |> ignore
            | e ->
                eprintfn $"Benchmark {e} is not supported. Choose between create, search"
        | "sample" ->
            ()
        | "test" ->
            let func = results.GetResult Arguments.Function
            match func with
            | "pickle" ->
                let p = PickleBenchmarks()
                p.V1WriteToDisk ()
            | "unpickle" ->
                let p = PickleBenchmarks()
                p.V1ReadFromDisk ()
                printfn $"Read k-d tree from disk! Doing nearest neighbor search.."
                let point = nearestNeighbor p.IdxTree { X = 442708.0; Y = 7525958.5 }
                printfn $"Found point {point.Value}!"
            | e ->
                eprintfn $"Test {e} is not supported. Choose between pickle and unpickle. See --help for more"
        | _ ->
            printfn "%s" (parser.PrintUsage())
    with e ->
        eprintfn $"{e.Message}"

    exit 0
