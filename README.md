# FsKDTree

A static k-d tree implementation in F#.

But for now just a 2-d tree

## Build

`dotnet run`

## Package

`dotnet run pack`

# Usage

```
type YourDataType = int
let points: Leaf<float, YourDataType> array = [|
    { Pos = { X = 10.; Y = 10. }; Data = 0 }
    { Pos = { X = 20.; Y = 20. }; Data = 1 }
    { Pos = { X = 30.; Y = 30. }; Data = 2 }
|]
let create2DTree = FsKDTree.create2DTree (LeafNodeSize 64)

let tree = create2DTree points
// Do a range search with a bounding box
let res = rangeSeach tree 10. 10. 20. 20.
// Some([{ X = 10.; Y = 10.; Idx = 0 }; { X = 20.; Y = 20.; Idx = 1 }])
```

You can also take a look at `src/cli/Program.fs` for an example of fetch NetCDF data and loading it into the k-d tree
