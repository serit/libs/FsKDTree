namespace FsKDTree

module V2 =

    type Tree<'LeafData, 'INodeData> =
        | LeafNode of 'LeafData
        | InternalNode of 'INodeData * (Tree<'LeafData, 'INodeData> * Tree<'LeafData, 'INodeData>)

    type LeafNodeSize = LeafNodeSize of int

    [<Struct>]
    type Pos<'a> = { X: 'a; Y: 'a }
    [<Struct>]
    type Leaf<'a, 'T> = { Pos: Pos<'a>; Data: 'T }

    [<Struct>]
    type Node<'a> = { Location: Pos<'a>; Axis: Axis }
    and Axis =
        | X = 0
        | Y = 1
        | Z = 2

    /// <summary>
    /// Creates a 2D tree with the given array of Leafs, which are types containing a Pos (X,Y), and some Data 'T. The Leaf
    /// Node Size refers to the fact that the leaves in this tree consists of sequences.
    ///
    /// Is this function tail recursive?
    /// </summary>
    let create2DTree (leafNodeSize: LeafNodeSize) (points: Leaf<'a, 'T> array) =
        let (LeafNodeSize ns) = leafNodeSize
        // NOTE(SimenLK): It's a 2 dimensional tree!
        let dims = 2

        let rec inner (points': Leaf<'a, 'T> array) (depth: int) =
            let pointsLength = Array.length points'
            // NOTE(SimenLK): A leaf is a list containing nodeSize amount of points
            if pointsLength <= ns then
                LeafNode points'
            else
                let axis = enum<Axis> (depth % dims)

                let sortedPoints =
                    points'
                    |> Array.sortBy (fun p ->
                        match axis with
                        | Axis.X -> p.Pos.X
                        | Axis.Y -> p.Pos.Y
                        | _ -> failwith "Only 2 dimensions work at the moment")
                let median = int (float pointsLength * 0.5)
                let medianPoint = Array.item median sortedPoints
                let left, right = Array.splitAt median sortedPoints

                InternalNode({ Location = medianPoint.Pos; Axis = axis }, (inner left (depth + 1), inner right (depth + 1)))

        // NOTE(SimenLK): Start a inner recursive loop with a start depth of 0
        inner points 0
