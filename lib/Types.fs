namespace FsKDTree

[<AutoOpen>]
module Types =
    type Tree<'LeafData, 'INodeData> =
        | LeafNode of 'LeafData
        | InternalNode of 'INodeData * (Tree<'LeafData, 'INodeData> * Tree<'LeafData, 'INodeData>)

    type LeafNodeSize = LeafNodeSize of int
    // TODO(SimenLK): have the user define the pos type?
    // TODO(simkir): use arrays to make it k dimensional
    [<Struct>]
    type Pos<'a> = { X: 'a; Y: 'a }
    [<Struct>]
    type Leaf<'a, 'T> = { Pos: Pos<'a>; Data: 'T }

    [<Struct>]
    type Node<'a> = { Location: Pos<'a>; Axis: Axis }
    and Axis =
        | X = 0
        | Y = 1
        | Z = 2
