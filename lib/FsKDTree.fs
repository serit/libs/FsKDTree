namespace FsKDTree

[<AutoOpen>]
module FsKDTree =

    open Microsoft.FSharp.Core
    open FSharpPlus
    open MBrace.FsPickler

    let rec printRootNode tree =
        match tree with
        | LeafNode (leafList: 'T array) ->
            let median = Array.length leafList / 2
            let node = leafList[median]
            printfn $"Median node of LeafNode with PointList {node}"
        | InternalNode (node, _) -> printfn $"Root Internal node: {node}"

    type private IdxTree<'a, 'T> = Tree<Leaf<'a, 'T> array, Node<'a>>
    let toBytes<'a, 'T> (tree: IdxTree<'a, 'T>) =
        let binarySerializer = FsPickler.CreateBinarySerializer ()
        binarySerializer.Pickle tree

    let fromBytes<'a, 'T> (bytes: byte array) =
        let binarySerializer = FsPickler.CreateBinarySerializer ()
        binarySerializer.UnPickle<IdxTree<'a, 'T>> bytes


    /// <summary>
    /// Creates a 2D tree with the given array of Leafs, which are types containing a Pos (X,Y), and some Data 'T. The Leaf
    /// Node Size refers to the fact that the leaves in this tree consists of sequences.
    /// </summary>
    let create2DTree (leafNodeSize: LeafNodeSize) (points: Leaf<'a, 'T> array) =
        let (LeafNodeSize ns) = leafNodeSize
        // NOTE(SimenLK): It's a 2 dimensional tree!
        let dims = 2

        let rec inner (points': Leaf<'a, 'T> array) (depth: int) =
            let pointsLength = Array.length points'
            // NOTE(SimenLK): A leaf is a list containing nodeSize amount of points
            if pointsLength <= ns then
                LeafNode points'
            else
                let axis = enum<Axis> (depth % dims)

                let sortedPoints =
                    points'
                    |> Array.sortBy (fun p ->
                        match axis with
                        | Axis.X -> p.Pos.X
                        | Axis.Y -> p.Pos.Y
                        | _ -> failwith "Only 2 dimensions work at the moment")
                let median = int (float pointsLength * 0.5)
                let medianPoint = sortedPoints[median]
                let left, right = Array.splitAt median sortedPoints

                InternalNode({ Location = medianPoint.Pos; Axis = axis },
                             (inner left (depth + 1), inner right (depth + 1)))

        // NOTE(SimenLK): Start a inner recursive loop with a start depth of 0
        inner points 0

    let convert<'T> (x: double): 'T =
        match box Unchecked.defaultof<'T> with
        | :? double -> double x |> box
        | :? single -> single x |> box
        | _ -> float x |> box
        :?> 'T

    let inline posLength pos =
        let two = LanguagePrimitives.GenericOne + LanguagePrimitives.GenericOne

        pos.X ** two + pos.Y ** two
        |> sqrt

    let inline posLength' pos =
        pos.X * pos.X + pos.Y * pos.Y
        |> sqrt

    /// <summary>
    /// Find the distance between two positions. Currently only in 2 dimensions.
    /// </summary>
    let inline posDistance' (fromPos: ^a Pos) (toPos: ^a Pos) =
        let fromX = float fromPos.X
        let fromY = float fromPos.Y
        let toX = float toPos.X
        let toY = float toPos.Y
        sqrt (
            (fromX - toX) * (fromX - toX)
            + (fromY - toY) * (fromY - toY)
        )

    /// <summary>
    /// Find the squared distance between two positions. Currently only in 2 dimensions.
    /// </summary>
    let inline posDistanceSq' (fromPos: ^a Pos) (toPos: ^a Pos) =
        let xAxis = fromPos.X - toPos.X
        let xAxisSq = xAxis * xAxis
        let yAxis = fromPos.Y - toPos.Y
        let yAxisSq = yAxis * yAxis

        xAxisSq + yAxisSq

    /// <summary>
    /// Find the distance between two positions. Currently only in 2 dimensions.
    /// </summary>
    let inline posDistance (fromPos: ^a Pos) (toPos: ^a Pos) =
        let two = LanguagePrimitives.GenericOne + LanguagePrimitives.GenericOne
        let a = fromPos.X ** two + toPos.X ** two + fromPos.Y ** two + toPos.Y ** two
        let b = two * (fromPos.X * toPos.X + fromPos.Y * toPos.Y)
        sqrt (a - b)

    /// <summary>
    /// Find the squared distance between two positions. Currently only in 2 dimensions.
    /// </summary>
    let inline posDistanceSq (fromPos: ^a Pos) (toPos: ^a Pos) =
        let two = LanguagePrimitives.GenericOne + LanguagePrimitives.GenericOne
        let a = fromPos.X ** two + toPos.X ** two + fromPos.Y ** two + toPos.Y ** two
        let b = two * (fromPos.X * toPos.X + fromPos.Y * toPos.Y)

        a - b


    /// <summary>
    /// Search for _the_ nearest neighbor to the given point.
    ///
    /// TODO(simkir):
    ///     - Use a hyperrect
    ///     - Add priority queue to find many neighbors
    ///
    /// <returns>
    /// A Leaf option
    /// </returns>
    /// </summary>
    let inline nearestNeighbor (tree: Tree<Leaf<'a, 'T> array, Node<'a>>) (point: Pos<'a>) =
        let maxValue = getMaxValue ()

        let rec inner tree (startDist, startP) =
            match tree with
            | LeafNode [||] ->
                // NOTE(SimenLK): The list is empty
                (startDist, None)
            | LeafNode points ->
                // NOTE(SimenLK): fold through this leaf's points, and find the
                //  point with the shortest distance to the input point.
                let bestDist, bestP =
                    Array.fold
                        (fun (bestDist, bestP) l ->
                            let newDist =
                                posDistance l.Pos point //|> convert<'a>
                            if newDist < bestDist then
                                (newDist, Some l)
                            else
                                (bestDist, bestP))
                        (startDist, startP)
                        points

                (bestDist, bestP)
            | InternalNode (node, (leftChild, rightChild)) ->
                // NOTE(SimenLK): Find the correct child to traverse down, but remember the other choice
                let child, other =
                    match node.Axis with
                    | Axis.X when point.X <= node.Location.X -> leftChild, rightChild
                    | Axis.Y when point.Y <= node.Location.Y -> leftChild, rightChild
                    | _ -> rightChild, leftChild

                let nodeDist =
                    posDistance node.Location point //|> convert<'a>
                // TODO(simkir): Can the nodeDist be shorter than the distance to the best possible point? Hmm
                // NOTE(simkir): This passes my simple test, but not sure in larger grids
                if nodeDist < startDist then
                    let currentDist, currentP = inner child (startDist, startP)
                    let otherDist, otherP = inner other (currentDist, currentP)
                    if otherDist < currentDist then
                        otherDist, otherP
                    else
                        currentDist, currentP
                else
                    startDist, startP

        inner tree (maxValue, None) |> snd

    let rec rangeSearch tree minPos maxPos : Leaf<'a, 'b> array =
        let inline checkAxis node child pos cmpFunc =
            match node.Axis with
            | Axis.X when cmpFunc pos.X node.Location.X -> rangeSearch child minPos maxPos
            | Axis.Y when cmpFunc pos.Y node.Location.Y -> rangeSearch child minPos maxPos
            | _ -> [||]

        match tree with
        | LeafNode points ->
            // NOTE(SimenLK): do a linear search when a leaf is found
            points
            |> Array.filter (fun leaf ->
                let pos = leaf.Pos
                pos.X >= minPos.X
                && pos.X <= maxPos.X
                && pos.Y >= minPos.Y
                && pos.Y <= maxPos.Y)

        | InternalNode (node, (leftChild, rightChild)) ->
            let left = checkAxis node leftChild minPos (fun x y -> x <= y)
            let right = checkAxis node rightChild maxPos (fun x y -> x >= y)

            Array.append left right

    /// This does not work, need to use a hyperrect?
    /// TODO(simkir): Add hyperrect here aswell?
    let inline radialSearch
            (tree: Tree<Leaf<'a, _> array, Node<'a>>)
            radius
            (pos: ^a Pos)
        =
        let rec inner tree =
            match tree with
            | LeafNode (points: Leaf<'a, _> array) ->
                points
                |> Array.filter (fun leaf ->
                    let len = posDistance' pos leaf.Pos
                    printfn $"{pos} - {leaf.Pos} = {len}"
                    len <= radius)

            | InternalNode (node, (leftChild, rightChild)) ->
                let left =
                    match node.Axis with
                    | Axis.X when pos.X <= node.Location.X -> inner leftChild
                    | Axis.Y when pos.Y <= node.Location.Y -> inner leftChild
                    | _ -> [||]
                let right =
                    match node.Axis with
                    | Axis.X when node.Location.X >= pos.X -> inner rightChild
                    | Axis.Y when node.Location.Y >= pos.Y -> inner rightChild
                    | _ -> [||]

                Array.append left right

        inner tree
