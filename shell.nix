with import <nixpkgs> {};
mkShell {
  buildInputs = [
    dotnet-sdk_6
    netcdf
    nodejs
  ];

  DOTNET_ROOT = "${dotnet-sdk_6}";
  shellHook = ''
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${stdenv.cc.cc.lib}/lib;
  '';
}
