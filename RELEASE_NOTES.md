# Changelog

## [3.0.4](https://gitlab.com/serit/libs/FsKDTree/compare/v3.0.3...v3.0.4) (2022-09-07)


### Bug Fixes

* don't use dotnet restore lock file ([9653692](https://gitlab.com/serit/libs/FsKDTree/commit/965369214da3f20c0dcc8362b90b327e87843ebe))

## [3.0.3](https://gitlab.com/serit/libs/FsKDTree/compare/v3.0.2...v3.0.3) (2022-09-07)


### Bug Fixes

* reorganize dist expression for numerical stability. ([6fb7938](https://gitlab.com/serit/libs/FsKDTree/commit/6fb7938ad9a65e0e1ff0330bd9be52f67d6d0874))

## [3.0.2](https://gitlab.com/serit/libs/FsKDTree/compare/v3.0.1...v3.0.2) (2022-07-07)

## [3.0.1](https://gitlab.com/serit/libs/FsKDTree/compare/v3.0.0...v3.0.1) (2022-07-06)

# [3.0.0](https://gitlab.com/serit/libs/FsKDTree/compare/v2.0.1...v3.0.0) (2022-07-06)

## [2.0.1](https://gitlab.com/serit/simkir/FsKDTree/compare/v2.0.0...v2.0.1) (2022-06-16)

# [2.0.0](https://gitlab.com/serit/simkir/FsKDTree/compare/v1.1.0...v2.0.0) (2022-03-23)

# [1.1.0](https://gitlab.com/serit/simkir/KDTree/compare/v1.0.0...v1.1.0) (2022-03-23)


### Features

* make Point values generic ([37c5c2b](https://gitlab.com/serit/simkir/KDTree/commit/37c5c2b7dfc5b34ce5965384fd64b55bbc25d976))

# 1.0.0 (2022-03-21)


### Bug Fixes

* bad entry in sln file ([ce016f0](https://gitlab.com/serit/simkir/KDTree/commit/ce016f03ae8d806c4721c3acd3c297718636363a))
* change package name ([e8e9305](https://gitlab.com/serit/simkir/KDTree/commit/e8e9305185abb99521e33b15d6e218749ddb48b2))
* cicd and refactor (massive) ([eea209b](https://gitlab.com/serit/simkir/KDTree/commit/eea209b09c6d87ca3ace89cf305ad605c2a706a3))
* prefix version with ski to avoid confusion ([619a15c](https://gitlab.com/serit/simkir/KDTree/commit/619a15c4ea6d8827e7c4868431f8a744c6ff8fab))
* refactor tests ([94ab337](https://gitlab.com/serit/simkir/KDTree/commit/94ab337953d267d51c4593864fe67862ec1f23f2))
* set deploy name and kube namespace ([ac1e002](https://gitlab.com/serit/simkir/KDTree/commit/ac1e002510bf8a03ed5a04bb0c6e073634c96562))
* update and re enable tests ([24919ea](https://gitlab.com/serit/simkir/KDTree/commit/24919eaa882c8c058531a75f38127224462fc056))


### Features

* add rangeSearch ([e3a7787](https://gitlab.com/serit/simkir/KDTree/commit/e3a77873509d208ddd6f3353d665cbc67514de69))

## [1.0.1](https://gitlab.com/oceanbox/FSharp.KDTree/compare/v1.0.0...v1.0.1) (2022-03-20)


### Bug Fixes

* use oceanbox cicd ([b1fc420](https://gitlab.com/oceanbox/FSharp.KDTree/commit/b1fc420ddf7161f5c666e493d719c2cef8f80c1b))

# 1.0.0 (2022-03-20)


### Bug Fixes

* add missing builder ([dc67fc0](https://gitlab.com/oceanbox/FSharp.KDTree/commit/dc67fc0162fd425c634e48d7eb4a781e9e1383a6))
