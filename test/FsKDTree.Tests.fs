module Tests

open Expecto
open FsKDTree

let posWithIdxToLeaf (idx: int) (pos: Pos<'a>) : Leaf<'a, int> =
    {
        Pos = pos
        Data = idx
    }

let sampleFloatPoints =
    [| { X = 30.; Y = 30. }; { X = 40.; Y = 40.} ; { X = 40.; Y = 45.}
       { X = 15.; Y = 10. }; { X = 15.; Y = 15.} ; { X = 25.; Y = 20.}
       { X =  5.; Y =  5. }; { X = 10.; Y =  5.} ; { X =  3.; Y =  5.}
       { X = 55.; Y = 50. }; { X = 50.; Y = 55.} ; { X = 53.; Y = 55.}
       { X = 10.; Y = 10. }; { X = 15.; Y =  5.} ; { X = 20.; Y = 20.} |]
    |> Array.mapi posWithIdxToLeaf

let leafNodeSize: LeafNodeSize = LeafNodeSize 4

[<Tests>]
let kdtree =
    testList "general" [

        testCase "mid point integers" <| fun _ ->
            let points =
                [ { X = 10; Y = 10 }; { X = 20; Y = 20 }; { X = 30; Y = 30 } ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            match tree with
            | InternalNode (node, _) ->
                let point = { X = 20; Y = 20 }
                Expect.equal node.Location point "This trees location should be this"
            | LeafNode _ -> ()

        testCase "mid point floats" <| fun _ ->
            let points =
                [ { X = 10.; Y = 10. }; { X = 20.; Y = 20. }; { X = 30.; Y = 30. } ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            match tree with
            | InternalNode (node, _) ->
                let point = { X = 20.; Y = 20. }
                Expect.equal node.Location point "This trees location should be this"
            | LeafNode _ -> ()

        testCase "mid point singles" <| fun _ ->
            let points =
                [
                    { X = single 10.; Y = single 10. }
                    { X = single 20.; Y = single 20. }
                    { X = single 30.; Y = single 30. }
                ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            match tree with
            | InternalNode (node, _) ->
                let point = { X = single 20.; Y = single 20. }
                Expect.equal node.Location point "This trees location should be this"
            | LeafNode _ -> ()

        testCase "mid float point of many" <| fun _ ->
            let tree = create2DTree leafNodeSize sampleFloatPoints
            match tree with
            | InternalNode (node, _) ->
                let point = { X = 20.; Y = 20. }
                Expect.equal node.Location point "This trees location should be this"
            | LeafNode _ -> ()

        testCase "range search small and easy" <| fun _ ->
            let points =
                [
                    { X = 10.; Y = 10. }
                    { X = 20.; Y = 20. }
                    { X = 30.; Y = 30. }
                ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            let expected = [
                { Pos = { X = 10.; Y = 10. }; Data = 0 }
            ]
            let res = rangeSearch tree { X = 0.; Y = 0. } { X = 10.; Y = 10. } |> List.ofSeq
            match res with
            | [] -> ()
            | ps ->
                Expect.containsAll
                    ps
                    expected
                    "range search of (0, 0), (10, 10) should yield these points"

        testCase "range search miss" <| fun _ ->
            let points =
                [
                    { X = 10.; Y = 10. }
                    { X = 20.; Y = 20. }
                    { X = 30.; Y = 30. }
                ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            let expected = [|  |]
            let res = rangeSearch tree { X = 31.; Y = 30 } { X = 40.; Y = 40.} |> Array.ofSeq
            Expect.equal res expected "range search of (30., 30.), (40., 40.) should miss all points"

        testCase "range search miss within" <| fun _ ->
            let points =
                [
                    { X = 10.; Y = 10. }
                    { X = 20.; Y = 20. }
                    { X = 30.; Y = 30. }
                ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            let expected = [|  |]
            let res = rangeSearch tree { X = 15.; Y = 15 } { X = 19.; Y = 19. } |> Array.ofSeq
            Expect.equal res expected "range search of (30., 30.), (40., 40.) should miss"

        testCase "range search multiple leafs" <| fun _ ->
            let tree = create2DTree leafNodeSize sampleFloatPoints
            let expected = [
                { Pos = { X = 30.; Y = 30. }; Data = 0 }
                { Pos = { X = 40.; Y = 40. }; Data = 1 }
            ]
            let res = rangeSearch tree { X = 10.; Y = 30. } { X = 50.; Y = 40. } |> Array.ofSeq
            match res with
            | [|  |] -> ()
            | ps ->
                Expect.containsAll
                    ps
                    expected
                    "range search of (30, 10), (40, 50) should yield these points"

        testCase "nearest neighbor obv" <| fun _ ->
            let tree = create2DTree leafNodeSize sampleFloatPoints
            let point = { X = 5.5; Y = 5.5 }
            let expected = Some { Pos = { X = 5.; Y = 5. }; Data = 6 }
            let res = nearestNeighbor tree point
            Expect.equal res expected "The nearest neighbor of (3, 5) should be (5, 5)"

        test "nearest neighbor obv 1" {
            let tree = create2DTree leafNodeSize sampleFloatPoints
            let point = { X = 23.; Y = 23. }
            let expected = Some { Pos = { X = 25.; Y = 20. }; Data = 5 }
            let res = nearestNeighbor tree point
            Expect.equal res expected "The nearest neighbor of (25, 20) should be (20, 20)"
        }

        test "nearest neighbor small" {
            let points =
                [ { X = 10.0f; Y = 10.0f }; { X = 20.0f; Y = 20.0f }; { X = 30.0f; Y = 30.0f } ]
                |> Array.ofList
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree leafNodeSize points
            let point = { X = 23.0f; Y = 23.0f }
            let expected = Some { Pos = { X = 20.0f; Y = 20.0f }; Data = 1 }
            let res = nearestNeighbor tree point
            Expect.equal res expected "The nearest neighbor of (25, 20) should be (20, 20)"
        }

    ]

let radialSearchTests =
    testList "radialSearch" [
        testCase "mid point integers" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let points =
                [| { X = 10; Y = 10 }; { X = 20; Y = 20 }; { X = 30; Y = 30 } |]
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree points
            let expected = [| { Pos = { X = 20; Y = 20 }; Data = 1 } |]
            let res = radialSearch tree 2 { X = 19; Y = 19 } 

            Expect.containsAll res expected
                "Radial search at point (19, 19) with a radius of 1.0 should yield point (20, 20)"
        )

        testCase "mid point float" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let points =
                [| { X = 10.0; Y = 10.0 }; { X = 20.0; Y = 20.0 }; { X = 30.0; Y = 30.0 } |]
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree points
            let expected : Leaf<float, int> array = [| { Pos = { X = 20.0; Y = 20.0 }; Data = 1 } |]
            let res = radialSearch tree 2.0 { X = 19.0; Y = 19.0 } 

            Expect.containsAll res expected
                "Radial search at point (19, 19) with a radius of 1.0 should yield point (20, 20)"
        )

        testCase "float mid point miss" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let points =
                [| { X = 10.0; Y = 10.0 }; { X = 20.0; Y = 20.0 }; { X = 30.0; Y = 30.0 } |]
                |> Array.mapi posWithIdxToLeaf
            let tree = create2DTree points
            let expected = [|  |]
            let res = radialSearch tree 1.0 { X = 19.0; Y = 19.0 } 

            Expect.containsAll res expected
                "Radial search at point (19, 19) with a radius of 1.0 should yield point (20, 20)"
        )

        testCase "float mid point right miss" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let tree = create2DTree sampleFloatPoints
            let expected = [|  |]
            let res = radialSearch tree 2.0 { X = 17.5; Y = 20.0 } 

            Expect.containsAll res expected
                "Radial search at point (17.5, 20.0) with a radius of 2.0 should miss"
        )

        testCase "float mid point right one" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let tree = create2DTree sampleFloatPoints

            let expected = [| { Pos = { X = 20.; Y = 20. }; Data = 5 } |]
            let radius = 3.0
            let res = radialSearch tree radius { X = 18.5; Y = 20.0 } 

            let test' = posDistance' { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let test = posDistance { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let testSq' = posDistanceSq' { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let testSq = posDistanceSq { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            printfn $"radial search pos distance: {test'}'({test' <= radius}) {test}({test <= radius})"
            printfn $"radial search pos distance sq: {testSq'}'({testSq' <= radius}) {testSq}({testSq <= radius})"

            Expect.containsAll res expected
                $"Radial search at point (18.1, 20.0) with a radius of {radius} should find point (20, 20)"
        )
    ]

let simpleTest =
    testList "Simple Tests" [
        testCase "mid float point of many" <| fun _ ->
            let tree = create2DTree leafNodeSize sampleFloatPoints
            match tree with
            | InternalNode (node, _) ->
                let point = { X = 20.; Y = 20. }
                Expect.equal node.Location point "This trees location should be this"
            | LeafNode _ -> ()
    ]

let simpleRadial =
    testList "Simple radial" [
        testCase "first" (fun _ ->
            let create2DTree = create2DTree leafNodeSize
            let tree = create2DTree sampleFloatPoints

            let expected = [| { Pos = { X = 20.; Y = 20. }; Data = 5 } |]
            let radius = 3.0
            let res = radialSearch tree radius { X = 18.5; Y = 20.0 } 

            let test' = posDistance' { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let test = posDistance { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let testSq' = posDistanceSq' { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            let testSq = posDistanceSq { X = 18.1; Y = 20.0 } { X = 20.; Y = 20. }
            printfn $"radial search pos distance: {test'}'({test' <= radius}) {test}({test <= radius})"
            printfn $"radial search pos distance sq: {testSq'}'({testSq' <= radius}) {testSq}({testSq <= radius})"

            Expect.containsAll res expected
                $"Radial search at point (18.1, 20.0) with a radius of {radius} should find point (20, 20)"
        )
    ]

let nearestNeighborFloatPoints =
    [|
        { X = 2.00; Y = 2.0 }
        { X = 2.00; Y = 5.0 }
        { X = 3.00; Y = 4.0 }
        { X = 5.00; Y = 5.0 }
        { X = 5.25; Y = 3.0 }
        { X = 5.50; Y = 3.5 }
        { X = 8.00; Y = 2.0 }
        { X = 8.00; Y = 5.0 }
        { X = 8.00; Y = 8.0 }
    |]
    |> Array.mapi posWithIdxToLeaf

let nearestNeighborSinglePoints =
    [|
        { X = 2.00f; Y = 2.0f }
        { X = 2.00f; Y = 5.0f }
        { X = 3.00f; Y = 4.0f }
        { X = 5.00f; Y = 5.0f }
        { X = 5.25f; Y = 3.0f }
        { X = 5.50f; Y = 3.5f }
        { X = 8.00f; Y = 2.0f }
        { X = 8.00f; Y = 5.0f }
        { X = 8.00f; Y = 8.0f }
    |]
    |> Array.mapi posWithIdxToLeaf

// TODO(SimenLK): Create more thorough tests
let nearestNeighbor =
    testList "Nearest neighbor" [
        testCase "nearest neighbor constructed" <| fun _ ->
            let tree = create2DTree leafNodeSize nearestNeighborFloatPoints
            let point = { X = 4.5; Y = 3.0 }
            let expected = Some { Pos = { X = 5.25; Y = 3.0}; Data = 4 }
            let res = nearestNeighbor tree point
            Expect.equal res expected $"The nearest neighbor of {point} should be {expected}"

        testCase "nearest neighbor singles" <| fun _ ->
            let tree = create2DTree leafNodeSize nearestNeighborSinglePoints
            let point = { X = 4.5f; Y = 3.0f }
            let expected = Some { Pos = { X = 5.25f; Y = 3.0f }; Data = 4 }
            let res = nearestNeighbor tree point
            Expect.equal res expected $"The nearest neighbor of {point} should be {expected}"
    ]

let all =
    testList "All" [ kdtree; radialSearchTests; nearestNeighbor ]

let simpleRadials =
    testList "Radial" [ simpleRadial; ]

[<EntryPoint>]
let main _ = runTestsWithCLIArgs [] [||] simpleRadials
